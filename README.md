The most distinctive fare in Frisco, featuring all-natural, locally-sourced meats and fresh produce. Delicious cocktails and microbrews. Happy hour daily with daily specials on appetizers and beverages. Enjoy fine dining ambiance with that special, Summit County touch. Reservations not required.

Address: 311 W Main Street, Frisco, CO 80443, USA

Phone: 970-455-8382